# Description
This is the code to perform W and top tagging with information from Lund Plane using Machine Learning methods. The idea was proposed in [arXiv:1807.04758](https://arxiv.org/abs/1807.04758) (and [talk_boost2018](https://indico.cern.ch/event/649482/contributions/2993312/attachments/1689020/2717225/talk_boost18.pdf)) and was explored in [HFSF2018](https://twiki.cern.ch/twiki/bin/viewauth/AtlasSandboxProtected/HFSF18#Top_and_W_tagging_using_Lund_Jet). 
In this package you will find two approaches to work out the idea: Recurrent neural network (RNN) and Convolutional neural network (CNN).

# Initial Setup
To run the codes in this package, it is necessary to set up [Jupyter](https://jupyter.org/) on your working machine.
- On Lxplus: You can set up Jupyter following the instructions [here](https://indico.cern.ch/event/615994/page/10686-lxplus-software-setup).
- On Linux: `bash Miniconda3-latest-Linux-x86_64.sh` to install Miniconda 3, with which you should be able to use Jupyter.
- On Windows (Mac, Linux): Download Anaconda [here](https://www.anaconda.com/download/#linux) and install it on your laptop.

# Inputs
Signal (W boson and top quark) and background (QCD dijets) Monte Carlo samples can be found [here](https://drive.google.com/file/d/198zTwv1OwXgoW66GMyeuHhziA3mjQMcv/view?usp=sharing). Input information is saved in a TTree, specifically, the information regarding Lund Plane is saved under the branch `lundjets_InDetTrackParticles`. Furthermore, the information in the TTree is saved in a multi-dimensional array, the structure of the array is built in this order (from top to bottom): Event -> Jets in each event -> Radiation pattern of each jet -> Variables of each radiation (definitions can be found in [arXiv:1807.04758](https://arxiv.org/abs/1807.04758)): $`\Delta R`$, $`Z`$, jet $`p_T`$, jet mass, jet ID and $`p_{T2}`$. 

# How to run
Download the package and set up the environment:
```
# Download package
git clone https://gitlab.cern.ch/tou/lund_plane_tagging_hfsf2018/

# Enter working directory
cd ipynb

# Set up environment (on lxplus)
source setup.sh

# Open Jupyter notebook on your browser, edit and run cells
```
## Plotting Lund Plane
Open `Lund_plane_plotting.ipynb`, modify input file paths, Lund Plane variables and jet $`p_T`$ cuts. Plot Lund Plane of signal and background for a sanity check.
## CNN
There are two pieces of CNN tagger code: `CNN_Lund_test.ipynb` and `CNN_Lund_test-3vars.ipynb`. In the former, two variables of radiation patterns are used: $`\ln (1/\Delta R)`$ and $`\ln (p_T\cdot\Delta R)`$. In the latter, an additional variable is taken into account: jet mass (or any other reasonable choice). You will get test results of the neural network saved as `.npy` files after running either of the notebooks.

With the test result files output from the last step, run `ROC_plotting.ipynb` to plot the Receiver operating characteristic (ROC) curve.
## RNN
In `RNN_Lund_test_allInOne.ipynb`, a `SimpleRNN` skeleton is implemented on Lund Jet plain dataset to discriminate the signal and background. In this test code, we use three observables $` (\ln (1/\Delta R),\,\ln (p_T\cdot\Delta R),\,\ln(1/z) )`$ to train RNN. Please re-specify the path to the datasets first and then you can run all cells to produce the test outputs. 

Also, please noted that you might spot some differences in the coding style of CNN and RNN implementation since they are set up individually. Just feel free to make your own style.

## Check your results
Compare the performance of your tagger with that of the [latest published taggers](http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/JETM-2018-03/).