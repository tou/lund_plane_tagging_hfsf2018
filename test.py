# This is the script implementing Keras tutorial:
# https://elitedatascience.com/keras-tutorial-deep-learning-in-python

import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.utils import np_utils
from keras.datasets import mnist
from matplotlib import pyplot as plt

np.random.seed(123)
(x_train,y_train),(x_test,y_test)=mnist.load_data()
print (x_train.shape)
plt.imshow(x_train[0])
#plt.show()

x_train=x_train.reshape(x_train.shape[0],1,28,28)
x_test=x_test.reshape(x_test.shape[0],1,28,28)
print (x_train.shape)
x_train=x_train.astype('float32')
x_test=x_test.astype('float32')
x_train/=255
x_test/=255

print (y_train.shape)
print (y_train[:10])

Y_train=np_utils.to_categorical(y_train,10)
Y_test=np_utils.to_categorical(y_test,10)
print (Y_train.shape)
print (Y_train)
print (Y_test)


model=Sequential()
model.add(Convolution2D(32,(3,3),activation='relu',input_shape=(1,28,28),data_format='channels_first'))
#print model.output_shape
model.add(Convolution2D(32, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(128,activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(10,activation='softmax'))

model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
model.fit(x_train,Y_train,batch_size=32,epochs=1,verbose=1)
score=model.evaluate(x_test,Y_test,verbose=0)
print (score)

